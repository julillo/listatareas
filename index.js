var express = require("express");
var bodyParser = require("body-parser");
var session = require("cookie-session");
var port = 3000;

var app = express();

app.use(session({secret: 'nodejs'}));
app.use(bodyParser.urlencoded({extended: false}));
app.set('view engine', 'ejs');

var tareas = [];

app.get('/', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);
    respuesta.render('formulario.ejs', {
        tareas: tareas
    });
});

app.post('/adicionar', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);
    var tarea = llamado.body.nuevaTarea;
    tareas.push(tarea);
    respuesta.redirect('/');
});

app.get('/borrar/:id', function (llamado, respuesta) {
    console.log(`[${Date()}] ${llamado.method} -> ${llamado.url}`);
    tareas.splice(llamado.params.id, 1);
    respuesta.redirect('/');
});

app.listen(port, function () {
    console.log("Corriendo en el puerto 3000");
});